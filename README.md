# generation of SPWM signals

Stuff lying around since I've repeaired a few power inverters...

This is everything I used to replace broken hardware.
I started with simulation and reading and reading hundreds of pages...

I the end I've built more than 10 discrete hv-h-bridges and played around alot
with integrated low voltage bridges like L6203, TDA.... cd-motor-drivers, ...
There is a lot of stuff out there capable of outputting SPWM...

Simulation-folders usually contain PNGs of everything, so you don't need to have
all this simulation software.

# SPWM and MSPWM in short

SPWM is a DDS technique to resemble a sine wave by outputting PWM pulses
related to amplitude of a sine wave.
It is used for example to generate AC from a DC-bus in power inverters.

Most likely a h bridge will be used for this job.

MSPWM drives two transistors with a 50hz rectangle, the other two with XX-kHz
SPWM signal.
This slowdown (of the upper 2) makes control/driving hardware cheap and easy.


# H Bridge numbering scheme
Wherever I refer to h-bridge transistors I use this numbering: 
TOP LEFT = 1 
BOTTOM LEFT = 2 
TOP RIGHT = 3 
BOTTOM RIGHT = 4 

This is a very common numbering scheme, so I adopted it. Beware, this might be
different in some linked references etc.

# stuff I "implemented" here / Software you need
- Atmel AVR (attiny13, atmega8) - avr-gcc, avrdude, make 
- Simulations using LTSpice 
- Simulations using SimulIDE 
- Simulations using Proteus 

# List of projects in here + short description
- ./ltspice__MSPWM-generator-asy/ - contains a MSPWM symbol that generates all needed signals. Can be edited for tuning parameters.
- ./ltspice__SPWM-comparator/ - Shows how to generate simple SPWM signals with a comparator and a triangle signal.

# References

