#include <stdio.h>
#include <inttypes.h>
#include <stdint.h>

int main(int argc, char **argv) {
	int runtime_seconds = 60;
	uint64_t runtime_periods = runtime_seconds * 100;

	double now_ms = 0.0;
	int voltage_high = 5000;
	int voltage_low = 0;
	int pol = 0;
	double turn_on_delay_ns = 100 * 0.000001;
	double rise_time_ns = 10 * 0.000001;
	double fall_time_ns = 10 * 0.000001;

	FILE *polarity = fopen("./polarity.txt", "w");
	FILE *npolarity = fopen("./npolarity.txt", "w");
//	fprintf(npolarity, "%fms\t", now_ms);
//	fprintf(npolarity, "%dmV\n", voltage_high);
	now_ms = 0.0;
	pol = 0;
	int voltage = 0;
	fprintf(polarity, "0ms\t0mV\n", 0);
	fprintf(npolarity, "0ms\t%dmV\n", voltage_high);


	while(runtime_periods) {
		if(voltage) {
			now_ms += 10.0;		// add half a period
			fprintf(polarity, "%fms\t", now_ms);
			fprintf(polarity, "%dmV\n", voltage_high);
			// add fall time
			fprintf(polarity, "%fms\t", now_ms+fall_time_ns);
			fprintf(polarity, "%dmV\n", voltage_low);


			fprintf(npolarity, "%fms\t", now_ms+turn_on_delay_ns);
			fprintf(npolarity, "%dmV\n", voltage_low);
			fprintf(npolarity, "%fms\t", now_ms+rise_time_ns+turn_on_delay_ns);
			fprintf(npolarity, "%dmV\n", voltage_high);

			voltage =0;
		} else {
			now_ms += 10.0;
			fprintf(polarity, "%fms\t", now_ms+turn_on_delay_ns);
			fprintf(polarity, "%dmV\n", voltage_low);
			fprintf(polarity, "%fms\t", now_ms+turn_on_delay_ns+rise_time_ns);
			fprintf(polarity, "%dmV\n", voltage_high);


			fprintf(npolarity, "%fms\t", now_ms);
			fprintf(npolarity, "%dmV\n", voltage_high);
			fprintf(npolarity, "%fms\t", now_ms+fall_time_ns);
			fprintf(npolarity, "%dmV\n", voltage_low);


			voltage = 1;
		
		}


	runtime_periods--;
	}
	fclose(polarity);
	fclose(npolarity);
}
