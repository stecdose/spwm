#include <stdio.h>
#include <math.h>
#include <inttypes.h>

int main(int argc, char **argv) {
    long unsigned int steps, i, rounded;    
    double d, scaled;
    int scale;
    
    steps = 360*2;
    scale = 0xff;
    
    
    printf("/* sine lookup table\n");
    printf(" *   characteristics of this wave:\n");
    printf(" *   amplitude\t%d\n", scale);
    printf(" *   type\thalf-wave\n");
    printf(" *   offset\tamplitude/2\n");
    printf(" *   #entries\t%lu\n", steps);
    printf(" */\n\n");
    
    printf("const uint8_t sine_lut[%lu] PROGMEM = {\n");
    
    
    for(i = 0; i < steps; i++) {
        d = sin(((M_PI)/steps)*i);
        
        if(d < 0)
            d *= -1.0;
        
        scaled = d*scale;
        
        rounded = (long unsigned int)scaled;
        if((scaled-rounded)>0.5)
            rounded++;
        
        printf("\t%f, %f, %lu\n", d, scaled, rounded);
        
        
    }
}
