#include <stdio.h>
#include <math.h>
#include <inttypes.h>

int main(int argc, char **argv) {
    long unsigned int steps, i, rounded;    
    double d, scaled;
    int scale;
    
    steps = 360*10;
    scale = 1023;
    
    
    printf("/* sine lookup table\n");
    printf(" *   characteristics of this wave:\n");
    printf(" *   amplitude\t%d\n", scale);
    printf(" *   type\thalf-wave\n");
    printf(" *   offset\tamplitude/2\n");
    printf(" *   #entries\t%lu\n", steps);
    printf(" */\n\n");
    
    printf("uint16_t sine_lut[%lu] = {\n", steps);
    
    
    for(i = 0; i < steps; i++) {
        d = sin(((2*M_PI)/steps)*i);
        if(d < 0)
            d *= -1.0;
        
        scaled = d*scale;
        rounded = (long unsigned int)scaled;
        if((scaled-rounded)>0.5)
            rounded++;
        
        printf("\t%lu", rounded);
        if(i == (steps-1))
            printf("\t// sample #%lu\n};\n\n", i);
        else
            printf(",\t// sample #%lu\n", i);
        
        
    }
}
